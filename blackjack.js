 const blackjackDeck = getDeck();

/**
 * Represents a card player (including dealer).
 * @constructor
 * @param {string} name - The name of the player
 */
//class CardPlayer { //TODO
class CardPlayer {
 constructor(name) {
   this.name = name;
   
  };
 hand = [];
 drawCard() {
   const randomCard = blackjackDeck[Math.floor(Math.random() * blackjackDeck.length)];
   this.hand.push(randomCard)
 };
};

// CREATE TWO NEW CardPlayers
//const dealer; // TODO
//const player; // TODO

const dealer = new CardPlayer('dealer');
const player = new CardPlayer('player');


/**
 * Calculates the score of a Blackjack hand
 * @param {Array} hand - Array of card objects with val, displayVal, suit properties
 * @returns {Object} blackJackScore
 * @returns {number} blackJackScore.total
 * @returns {boolean} blackJackScore.isSoft
 */
const calcPoints = (hand) => {
  // CREATE FUNCTION HERE
  // *** Had to look up how to do this (was a little confused)***
  let aces = 0;
  let total = 0;
  let isSoft = null;
// Returns total
  hand.forEach (card => {
    if (card.val == 11) {
      aces++
    }
    total += card.val
  });
// If/Else for aces count
  if (aces > 1) {
    aces = (aces - 1)
    total = (total - (aces * 10))
    isSoft = true;
  } else if (aces > 0 && total > 21) {
    total = total - 10
    isSoft = false;
  } else if (aces == 0) {
    isSoft = false;
  }
  return {total: total, isSoft: isSoft};
}

/**
 * Determines whether the dealer should draw another card.
 * 
 * @param {Array} dealerHand Array of card objects with val, displayVal, suit properties
 * @returns {boolean} whether dealer should draw another card
 */
const dealerShouldDraw = (dealerHand) => {
  // CREATE FUNCTION HERE
  if (calcPoints(dealerhand).isSoft && calcPoints(dealerHand).total) {
    return true;
  }
  else if (calcPoints(dealerHand).total <= 16); {
    return true;
  }
  return false;
}

/**
 * Determines the winner if both player and dealer stand
 * @param {number} playerScore 
 * @param {number} dealerScore 
 * @returns {string} Shows the player's score, the dealer's score, and who wins
 */
const determineWinner = (playerScore, dealerScore) => {
  // CREATE FUNCTION HERE
  if (playerScore === dealerScore) {
    return (`This game has ended in a tie. Both have a total score of ${playerScore}`)
  } else if (dealerScore > playerScore) {
    return (`The dealer has a total hand of ${dealerScore}\nYour total hand is ${playerScore}\n
    .You have lost`)
  } else {
    return (`CONGRATS! YOU WIN!! The dealer has a total hand of ${dealerScore}\nYour total hand is ${playerScore}`)
  }
}

/**
 * Creates user prompt to ask if they'd like to draw a card
 * @param {number} count 
 * @param {string} dealerCard 
 */
const getMessage = (count, dealerCard) => {
  return `Dealer showing ${dealerCard.displayVal}, your count is ${count}.  Draw card?`
}

/**
 * Logs the player's hand to the console
 * @param {CardPlayer} player 
 */
const showHand = (player) => {
  const displayHand = player.hand.map((card) => card.displayVal);
  console.log(`${player.name}'s hand is ${displayHand.join(', ')} (${calcPoints(player.hand).total})`);
}

/**
 * Runs Blackjack Game
 */
const startGame = function() {
  player.drawCard();
  dealer.drawCard();
  player.drawCard();
  dealer.drawCard();

  let playerScore = calcPoints(player.hand).total;
  showHand(player);
  while (playerScore < 21 && confirm(getMessage(playerScore, dealer.hand[0]))) {
    player.drawCard();
    playerScore = calcPoints(player.hand).total;
    showHand(player);
  }
  if (playerScore > 21) {
    return 'You went over 21 - you lose!';
  }
  console.log(`Player stands at ${playerScore}`);

  let dealerScore = calcPoints(dealer.hand).total;
  while (dealerScore < 21 && dealerShouldDraw(dealer.hand)) {
    dealer.drawCard();
    dealerScore = calcPoints(dealer.hand).total;
    showHand(dealer);
  }
  if (dealerScore > 21) {
    return 'Dealer went over 21 - you win!';
  }
  
  console.log(`Dealer stands at ${dealerScore}`);

// Additional logic if you draw 21 on 2 cards
  if (dealerScore === 21) {
    return (`The Dealer scored 21! Dealer wins!`)
  }
  if (playerScore === 21) {
    return (`You Win!!! You scored 21!`)
  }


  return determineWinner(playerScore, dealerScore);
}
// console.log(startGame());
console.log(startGame);