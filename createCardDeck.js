/**
 * Returns an array of 52 Cards
 * @returns {Array} deck - a deck of cards
 */
const getDeck = () => {
  const cards = [];
  const tenCards = ['jack', 'queen', 'king'];
  
  const suits =['hearts', 'clubs', 'diamonds', 'spades'];
  for (let suit of suits) {
    for (let i = 2; i <= 10; i++) {
      const card = {
        suit,
        val: i,
        displayVal: i.toString()
      };
      cards.push(card);
    }
  
    for (let tenCard of tenCards) {
      cards.push({
        suit,
        val: 10,
        displayVal: tenCard
      });
    }

    cards.push({
      suit,
      val: 11,
      displayVal: 'ace'
    });
   }

  return cards;
};

//****Invalid left-hand side expression in postfix operation****//

// CHECKS
// const deck = getDeck();
// console.log(`Deck length equals 52? ${deck.length === 52}`);

// const randomCard = deck[Math.floor(Math.random() * 52)];

// const cardHasVal = randomCard && randomCard.val && typeof randomCard.val === 'number';
// console.log(`Random card has val? ${cardHasVal}`);

// const cardHasSuit = randomCard && randomCard.suit && typeof randomCard.suit === 'string';
// console.log(`Random card has suit? ${cardHasSuit}`);

// const cardHasDisplayVal = randomCard &&
//   randomCard.displayVal &&
//   typeof randomCard.displayVal === 'string';
// console.log(`Random card has display value? ${cardHasDisplayVal}`);